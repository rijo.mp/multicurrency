const m2plog = require("../models/m2pRequestResponse");
const { performance } = require("perf_hooks");

let m2pDbServices = {
  saveLog(userID, name, url, t1, request, response) {
    return new Promise(async (resolve, reject) => {
      try {
        let t2 = performance.now();
        let temp = new m2plog();
        temp.userID = userID;
        temp.name = name;
        temp.url = url;
        temp.timeRequired = t2 - t1;
        temp.request = JSON.stringify(request);
        temp.timeStamp = new Date(Date.now());
        temp.response = JSON.stringify(response);
        await temp.save();
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  },
  saveErrorLog(userID, name, url, t1, request, error) {
    return new Promise(async (resolve, reject) => {
      try {
        let t2 = performance.now();
        let temp = new m2plog();
        temp.userID = userID;
        temp.name = name;
        temp.url = url;
        temp.timeRequired = t2 - t1;
        temp.request = JSON.stringify(request);
        temp.timeStamp = new Date(Date.now());
        temp.response = JSON.stringify(error);
        await temp.save();
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  },
};

module.exports = m2pDbServices;
