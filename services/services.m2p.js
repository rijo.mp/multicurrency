const axiosm2p = require("../interceptors/interceptor.m2p");

const m2pService = {
  getCardList(cpr, userID) {
    return new Promise(async (resolve, reject) => {
      try {
        const req = {
          entityId: cpr,
          userID,
        };
        const data = await axiosm2p.post("/business-entity-manager/v3/getCardList", req);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },

  register(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosm2p.post("/registration-manager/v4/register", req);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  fetchPreference(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosm2p.post("/business-entity-manager/fetchPreference", req);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  setPreference(req) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = axiosm2p.post("/business-entity-manager/updatePreferenceExternal", req);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  fetchBalance(cpr, userID) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosm2p.get(`/forex-manager/fetchAllBalance/${cpr}`, { params: { userID } });
        // console.log(data);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },

  changeCardStatus(req) {
    return new Promise(async (resolve, reject) => {
      try {
        //console.log("The payload is"+req)
        let data = await axiosm2p.post(`/business-entity-manager/block`, req);
        // console.log(data);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },

  getPCIWidgetUrl(payload) {
    return new Promise(async (resolve, reject) => {
      try {
        let type = payload.type;
        delete payload.type;
        let data = await axiosm2p.post(`/bitUrl/${type}`, payload);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  updateCustomer(payload) {
    return new Promise(async (resolve, reject) => {
      try {
        //console.log("The payload is"+req)
        let data = await axiosm2p.post(`/business-entity-manager/updateentity`, payload);
        // console.log(data);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  replaceCard(payload) {
    return new Promise(async (resolve, reject) => {
      try {
        //console.log("The payload is"+req)
        let data = await axiosm2p.post(`/business-entity-manager/replaceCard`, payload);
        // console.log(data);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },

  addMoney(payload) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosm2p.post("/txn-manager/create", payload);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
  updateConversionRate(payload) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosm2p.post(`/forex-manager/updateConversionRate`, payload);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    });
  },
};

module.exports = m2pService;
