const express = require("express");
const dotenv = require("dotenv").config();
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");

const cardRouter = require("./routes/card");
const userRouter = require("./routes/user");
const cxportalRouter = require("./routes/cxportal");

const app = express();
const { uploadExchangeRateCron } = require("./controllers/transactionsController");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

let db = {
  params: { useNewUrlParser: true, useUnifiedTopology: true }, //,useFindAndModify:false , useCreateIndex :true}
};
// console.log(process.env.mongoURL)
mongoose.connect(process.env.mongoURL, db.params, function (err) {
  if (err) console.log(err);
  else console.log("MongoDB Connected Successfully.");
});

app.use("/mc/card", cardRouter);
app.use("/mc/user", userRouter);
app.use("/mc/cxportal", cxportalRouter);
app.post("/mc/updateExchangeRateCron", uploadExchangeRateCron);

app.get("*", function (req, res) {
  res.send("Unknown Path Specified", 404);
});

app.post("*", function (req, res) {
  res.send("Unknown Path Specified", 404);
});

app.listen(process.env.PORT, function () {
  // console.log(process.env.NODE_ENV)
  console.log("Server Started At Port " + process.env.PORT + " and waiting for Requests..");
});

module.exports = app;
