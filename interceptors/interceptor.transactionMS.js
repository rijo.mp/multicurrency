const axios = require("axios");

const transactionMSAxios = axios.create({
  baseURL: process.env.transactionMSURL,
});

const requestHandler = async (request) => {
  return request;
};

const responseHandler = (response) => {
  if (response.status === 401) {
    // window.location = '/login';
  }
  return response.data;
};

const errorHandler = async (error) => {
  //write log to db
  return Promise.reject(error);
};

transactionMSAxios.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);

transactionMSAxios.interceptors.response.use(
  (response) => responseHandler(response),
  (error) => errorHandler(error)
);

module.exports = transactionMSAxios;
