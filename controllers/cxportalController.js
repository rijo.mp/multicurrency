const emailService = require("../services/service.email");
const Email = require("../classes/email.class");
const User = require("../models/userSchema");
const CommonDatas = require("../models/commonDatasSchema");
const Application = require("../models/applicationDataSchema");
const timeUtils = require("../utils/timeUtils");
const m2pRequestResponse = require("../models/m2pRequestResponse");
const { SendResponse } = require("../common/commonFunctions");
const kitNumbers = require("../models/kitNumberSchema");
const transactionService = require("../services/services.transactionMS");
const m2pService = require("../services/services.m2p");
const { convertCardDetailsFormat } = require("../utils/cardUtils");
const Card = require("../models/cardSchema");
const cardServices = require("../dbServices/cardServices");

const cxportalController = {
  getPendingList: async (req, res) => {
    console.log("wjdwd");
    let fromDate = req.body.fromDate,
      toDate = req.body.toDate;
    let test = true;
    let pendingList = [];
    if (fromDate && toDate && test) {
      console.log("Pending");
      let from = await timeUtils.getTimestamp(fromDate);
      let to = await timeUtils.getTimestamp(toDate);
      console.log(from, to);
      pendingList = await Application.find(
        {
          "applicationStatus.status": false,
          "applicationStatus.date": { $gt: from, $lt: to },
        },
        {
          CPR: 1,
          applicationStatus: 1,
          communicationInfo: 1,
          kitInfo: 1,
          firstName: 1,
          lastName: 1,
          _id: -1,
        }
      ).lean();
      console.log(pendingList);
    } else {
      pendingList = await Application.find(
        { "applicationStatus.status": false },
        {
          CPR: 1,
          applicationStatus: 1,
          communicationInfo: 1,
          kitInfo: 1,
          firstName: 1,
          lastName: 1,
          _id: -1,
        }
      ).lean();
    }
    console.log(pendingList);
    console.log("wjdwd");
    let refinedList = [];

    if (pendingList.length > 0) {
      pendingList.map(async (item) => {
        //refinedList.push(parseInt(item["CPR"]));
        let obj = {
          date: item["applicationStatus"]["date"] || "",
          cpr: parseInt(item["CPR"]),
          mobile: item["communicationInfo"][0]["contactNo"],
          email: item["communicationInfo"][0]["email"],
          name: item["firstName"] + " " + item["lastName"],
          multiCurrencyStatus: item["applicationStatus"]["status"],
          cardCategory: item["kitInfo"][0]["cardCategory"] || "",
        };
        refinedList.push(obj);
      });

      return res.status(200).json(refinedList);
    } else {
      return res.status(200).json({});
    }
  },
  getAllCards: async (req, res) => {
    console.log("ALL");
    try {
      let fromDate = req.body.fromDate,
        toDate = req.body.toDate;
      let from = new Date(fromDate);
      let to = new Date(toDate);
      let testList = await Application.aggregate([
        {
          $match: {
            "applicationStatus.time": {
              $gte: from,
              $lte: to,
            },
          },
        },
      ]).lookup({
        from: "cards",
        localField: "entityId",
        foreignField: "entityId",
        as: "cardData",
      });
      //console.log(testList)
      check = "-Date n time, CPR, Mobile Number, Email ID, Name and Multicurrency status ,cardCategory";
      let refinedList = testList.map((item) => {
        cardData = item.cardData[0] ? item.cardData[0]["cards"] : "";
        //console.log(cardData)
        cardInfo = cardData ? cardData[0] : "";
        comInfo = item.communicationInfo[0] ? item.communicationInfo[0] : "";
        console.log(cardInfo);
        return {
          date: item.applicationStatus.time || "",
          cpr: item.entityId,
          mobile: comInfo ? comInfo.contactNo : "",
          email: comInfo ? comInfo.emailId : "",
          name: item.firstName + " " + item.lastName,
          multiCurrencyStatus: cardInfo ? cardInfo.cardStatus : "NO CARD",
          cardType: cardInfo ? cardInfo.cardType : "NO CARD",
          // cardType : cardData?cardData[0]['cardType']:'',
        };
      });
      SendResponse(res, 200, "Customer List", { list: refinedList });
    } catch (err) {
      SendResponse(res, 500, err.message, null);
    }
  },
  getPendingUser: async (req, res) => {
    let cpr = req.body.cpr;
    let pendingCustomer = await Application.findOne({ CPR: cpr });
    return res.status(200).json(pendingCustomer);
  },
  pushUser: async (req, res) => {
    let registered = false,
      kitNo,
      applicationData;
    try {
      let cpr = req.body.cpr;
      applicationData = await Application.findOneAndUpdate(
        { entityId: cpr },
        {
          $inc: { "applicationStatus.retryCount": 1 },
          "applicationStatus.time": Date.now(),
        },
        { returnOriginal: false }
      );

      kitNo = await kitNumbers.findOneAndUpdate({ status: false, isInvalid: false }, { status: true, entityId: cpr }, { returnOriginal: false });
      if (!kitNo) return SendResponse(res, 500, "No Kit Numbers Available");
      // console.log(kitNo.kitNo);

      const m2pRequest = {
        entityId: applicationData.entityId,
        userID: applicationData.userID,
        businessId: applicationData.businessId,
        title: applicationData.title,
        firstName: applicationData.firstName,
        lastName: applicationData.lastName,
        gender: applicationData.gender,
        addressInfo: [
          {
            addressCategory: applicationData.addressInfo?.[0].addressCategory,
            address1: applicationData.addressInfo?.[0].address1,
            address2: applicationData.addressInfo?.[0].address2,
            address3: applicationData.addressInfo?.[0].address3,
            city: applicationData.addressInfo?.[0].city,
            state: applicationData.addressInfo?.[0].state,
            country: applicationData.addressInfo?.[0].country,
            pincode: applicationData.addressInfo?.[0].pincode,
          },
        ],
        communicationInfo: [
          {
            contactNo: applicationData.communicationInfo.contactNo,
            emailId: applicationData.communicationInfo.emailId,
          },
        ],
        dateInfo: [
          {
            dateType: applicationData.dateInfo.dateType,
            date: applicationData.dateInfo.date,
          },
        ],
        entityType: "CUSTOMER",
        businessType: "BFCFX",
        channelName: "MULTI_CURRENCY_NO_WAIVER",
        kitInfo: [
          {
            kitNo: `${kitNo.kitNo}`,
            cardType: "PHYSICAL",
            cardCategory: "PREPAID",
            cardRegStatus: "ACTIVE",
            aliasName: `${applicationData.firstName} ${applicationData.lastName}` || "",
          },
        ],
        kycInfo: [
          {
            documentType: "CPR",
            documentNo: cpr,
          },
        ],
      };

      // let transactionData = await transactionService.createTransaction("0", applicationData.userID);
      // console.log(transactionData.transactionID);
      const m2pResponse = await m2pService.register(m2pRequest);
      if (m2pResponse?.result?.kitNo === m2pRequest.kitInfo[0].kitNo) {
        // transactionData = await transactionService.updateTransaction(true, transactionData.transactionID);
        applicationData.applicationStatus.status = true;
        applicationData.applicationStatus.m2pResponse = JSON.stringify(m2pResponse.result);
        applicationData.applicationStatus.date = Date.now();
        await applicationData.save();
        registered = true;
        return SendResponse(res, 200, "CUSTOMER REGISTERED", {
          // transactionId: transactionData.transactionID,
          // transactionId: "25345345634",
          registered,
          entityId: m2pResponse.result.entityId,
          kitNo: m2pResponse.result.kitNo,
        });
      }
    } catch (err) {
      console.log(err.message);
      if (err.exception?.errorCode === "Y503") {
        applicationData.applicationStatus.status = true;
        applicationData.applicationStatus.date = Date.now();
        applicationData.applicationStatus.m2pResponse = JSON.stringify(err.exception);
        await applicationData.save();

        return SendResponse(res, 500, "Customer Already Registered");
      }
      if (err.exception?.errorCode === "Y2272") {
        applicationData.applicationStatus.date = Date.now();
        applicationData.applicationStatus.m2pResponse = JSON.stringify(err.exception);
        await applicationData.save();
        kitNo.isInvalid = true;
        await kitNo.save();

        return SendResponse(res, 500, "Invalid Kit Number");
      }
      return SendResponse(res, 500, "Internal Server Error");
    } finally {
      try {
        if (!registered && kitNo) {
          kitNo.status = false;
          kitNo.entityId = "";
          await kitNo.save();
        }
        if (registered && kitNo) {
          // if (!card) {
          //   card = await Card.create({
          //     entityId: req.body.cpr,
          //     CPR: req.body.cpr,
          //     userID: applicationData.userID,
          //     cards: [],
          //   });
          // }
          // const data = await m2pService.getCardList(req.body.cpr, applicationData.userID);
          // const cardsArray = convertCardDetailsFormat(data.result);
          // let user = await User.findOneAndUpdate({ CPR: req.body.cpr }, { status: cardsArray[0].cardStatus });
          // card.cards = cardsArray;
          // await card.save();
          let card = await Card.findOne({ CPR: req.body.cpr });
          let user = await User.findOne({ CPR: req.body.cpr });
          await cardServices.updateCarddataInDB(req.body.cpr, user.userID, user, card);
        }
      } catch (err) {
        console.log(err);
      }
    }
  },

  m2pLogs: async (req, res) => {
    try {
      console.log(req.params);
      const limitValue = 20;
      const skipValue = +req.params.pageno * limitValue || 0;
      const m2plogs = await m2pRequestResponse.find({ userID: req.params.userID }).skip(skipValue).limit(limitValue).sort({ timeStamp: -1 }).lean();
      // res.status(200).json(m2plogs);
      SendResponse(res, 200, "M2PLOGS", m2plogs);
    } catch (err) {
      console.log(err);
      SendResponse(res, 500, err.message, null);
    }
  },
  getCardStatus: async (req, res)=>{
    try{
      let cardDetails = await Card.findOne(
        {
          CPR:req.body.cpr
        }
        ,
        {
          _id:0,__v:0
        }
      ).lean();
      let crd = cardDetails.cards.filter((card)=>{
        console.log(card.cardStatus)
        return card.cardStatus != 'BLOCKED'
  
      })
      delete cardDetails.cards
      const {cards: _, ...resp} = cardDetails;
      resp.card = crd[0]
  
      //cardDetails.card = crd
      SendResponse(res, 200, '', resp);
    }
    catch(err){
      SendResponse(res, 500, 'No Card Found', null);
    }
    
  }
};
module.exports = cxportalController;
