const express = require("express");
const router = express.Router();

const cxportalController = require("../controllers/cxportalController");

router.post("/getPendingList", cxportalController.getPendingList);
router.post("/getAllCards", cxportalController.getAllCards);
router.post("/getPendingUser", cxportalController.getPendingUser);
router.post("/pushUser", cxportalController.pushUser);
router.post("/m2pLogs/:userID/:pageno", cxportalController.m2pLogs);
router.post("/getCardStatus", cxportalController.getCardStatus)

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

module.exports = router;
