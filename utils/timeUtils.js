const moment = require("moment");

const timeUtils = {
  getTimestamp: async (dateString) => {
    // switch (true) {
    //     case /([0-9]+)||([1])/.test(str):
    //       display("• Matched 'xyz' test");
    //       break;
    //     case /test/.test(str):
    //       display("• Matched 'test' test");
    //       break;
    //     case /ing/.test(str):
    //       display("• Matched 'ing' test");
    //       break;
    //     default:
    //       display("• Didn't match any test");
    //       break;
    //   }
    console.log();
    let dateObject = new Date(dateString);
    return dateObject.getTime();
  },
  convertDateFormat(date) {
    return new Promise((resolve, reject) => {
      try {
        const formats = {
          format1: "MM-DD-YYYY",
          format2: "MM/DD/YYYY",
          format3: "DD-MM-YYYY",
          format4: "DD/MM/YYYY",
          format5: "YYYY-MM-DD",
          format6: "YYYY/MM/DD",
          format7: "YYYY-DD-MM",
          format8: "YYYY/DD/MM",
          format9: "DD-MMM-YYYY",
        };
        for (let format of Object.values(formats)) {
          let newDate = moment(date.toUpperCase(), format, true);
          if (newDate.isValid()) {
            return resolve(newDate.format("YYYY-MM-DD"));
          }
        }
        reject(new Error("Invalid Date Format"));
      } catch (ex) {
        console.log(ex);
        reject(new Error("convertDateFormat Error"));
      }
    });
  },

  checkAnnualSubRenewal(date) {
    return new Promise((resolve, reject) => {
      try {
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDate();
        const checkdate = new Date(year + 1, month, day);
        if (checkdate <= new Date()) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (err) {
        console.log(err.message);
        reject(err);
      }
    });
  },
};

module.exports = timeUtils;
