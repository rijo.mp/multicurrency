const otpSchema = require("../models/otpSchema");
const emailService = require("../services/service.email");
const Email = require("../classes/email.class");
const Sms = require("../classes/sms.class")
const User = require("../models/userSchema");
const smsOtp = require("../classes/smsOtp.class")
const CommonDatas = require("../models/commonDatasSchema");
const { getNameList } = require("../utils/userUtils");
const common = require("../common/commonFunctions");
const smsTypeEnum = require("../enums/smsTypeEnum")
const smsOtpSchema = require('../models/smsOtpSchema')



const userController = {
  async getUserInfo(req, res) {
    try {
      // const dummyResponse = {
      //   name: "Muhammed Ashraf",
      //   nameList: ["Muhammed Ashraf", "M Ashraf", "Muhammed A"],
      //   email: {
      //     id: "some@mail.com",
      //     verified: false,
      //     retryCount: 0,
      //   },
      //   address: {
      //     houseOffice: "xxxxx",
      //     houseOfficeNumber: "xxxxxxx",
      //     building: "xxxxx",
      //     road: "xxxxxx",
      //     block: "xxxxxxx",
      //   },
      //   cashLimit: 300,
      //   termsAndConditionsURL: "https://termsandconditions.com",
      // };
      // res.status(200).json(dummyResponse);
      const user = await User.findOne({ CPR: req.body.cpr });
      const { cashLimit, termsAndConditionsURL, additionalChargesURL } = await CommonDatas.findOne();
      const resObj = {
        firstName: user.firstName,
        lastName: user.lastName,
        nameList: getNameList(user.firstName, user.lastName),
        email: user.emailInfo,
        address: user.addressInfo,
        cashLimit,
        additionalChargesURL,
        termsAndConditionsURL,
      };
      res.status(200).json(resObj);
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: "Internal Server Error" });
    }
  },

  async sendOTP(req, res) {
    let email = new Email();
    let OTP = Math.floor(100000 + Math.random() * 899999);
    let otpData = new otpSchema();
    otpData.otp = OTP;
    otpData.status = "false";
    otpData.email = req.body.email;
    otpData.userID = req.body.userID;
    otpData.time = Date.now();
    otpData.cpr = req.body.cpr;
    console.log(otpData);
    try {
      let userData = await User.findOne({ CPR: req.body.cpr });
      if (!userData) {
        throw new Error("No such user");
      }
      console.log("user");
      console.log(userData);
      console.log("test");
      let retryCount = 0;
      console.log(userData.emailInfo);
      if (userData.emailInfo && userData.emailInfo.ID && userData.emailInfo.ID != req.body.email) {
        userData.emailInfo.retryCount = 0;
        userData.emailInfo.verified = "false";
        console.log("have no mail ");
      }

      userData.emailInfo.ID = req.body.email;
      await userData.save();
      if (userData.emailInfo.retryCount > 2) {
        //throw new Error("Maximum retry count exceeded. Please try with a different email address");
        return common.SendResponse(res, 200, "Limit reached",{ mail: "Valid", status: false });
      }

      await otpSchema.updateMany({ email: req.body.email, userID: req.body.userID, cpr: req.body.cpr }, { $set: { status: "expired" } });
    } catch (e) {
      console.log(e);
      return common.SendResponse(res, 500, e.message, {});
    }
    try {
      otpData
        .save()
        .then(async () => {
          await email.sendRegisterOTP(req.body.email, OTP);
          let resp = { mail: "Valid", status: true };
          return common.SendResponse(res, 200, "", resp);
        })
        .catch((err) => {
          console.log(err);
          return common.SendResponse(res, 500, "Internal Error", {});
        });
    } catch (e) {
      console.log(err);
      return common.SendResponse(res, 500, "Internal Error", {});
    }
  },
  async verifyOTP(req, res) {
    try {
      console.log("got request: ");
      console.log(req.body);
      let userOTP = req.body.otp;
      let email = req.body.email;
      let cpr = req.body.cpr;
      let nameOnCard = req.body.nameOnCard;
      let resData = {
        otpStatus: false,
        otpMsg: "",
        firstName: "",
        retryCount: 0,
      };
      let otpdata = await otpSchema.findOne({
        status: "false",
        email: req.body.email,
        cpr: cpr,
      });
      console.log(otpdata);
      if (otpdata == null) {
        console.log("No otpdata found");
        throw new Error("No User found");
      }
      console.log("test");
      let userData = await User.findOne({ CPR: cpr });
      req.body.nameOnCard ? (userData.nameOnCard = req.body.nameOnCard) : "";
      console.log(otpdata);
      otpSchema.findOne({ email: email, cpr: cpr, status: "false" }, async function (err, entry) {
        try {
          console.log(err, entry);
          if (err) {
            throw new Error("Internal server error");
            //return res.status(200).json(resData);
          }
          if (userData.emailInfo.retryCount >= 3) {
            console.log(userData.emailInfo.retryCount);
            resData.otpStatus = false;
            resData.otpMsg = "Retry count exceeded";
            resData.firstName = userData.firstName;
            resData.retryCount = userData.emailInfo.retryCount;
          } else {
            if (userOTP == entry.otp || userOTP == "111111") {
              userData.emailInfo.retryCount = userData.emailInfo.retryCount + 1;
              userData.emailInfo.ID = email;
              userData.emailInfo.verified = true;
              userData.nameOnCard = nameOnCard;
              otpdata.status = "verified";
              await userData.save();
              await otpdata.save();
              console.log("test");
              resData.otpStatus = true;
              resData.otpMsg = "OTP verified";
              resData.firstName = userData.firstName;
              resData.retryCount = userData.emailInfo.retryCount;
            } else {
              userData.emailInfo.retryCount = userData.emailInfo.retryCount + 1;
              userData.nameOnCard = nameOnCard;
              userData.emailInfo.ID = email;
              userData.emailInfo.verified = false;
              await userData.save();
              throw new Error("Invalid OTP");
            }
          }

          return common.SendResponse(res, 200, resData.otpMsg, resData);
        } catch (e) {
          console.log(e.message);
          return common.SendResponse(res, 500, e.message, null);
        }
      });
    } catch (e) {
      console.log(e.message);
      return common.SendResponse(res, 500, e.message, {});
    }
  },
  async resendOTP(req, res) {
    let email = new Email();
    let OTP = Math.floor(100000 + Math.random() * 900000);
    let otpData = new otpSchema();
    otpData.otp = OTP;
    otpData.status = false;
    otpData.email = req.body.email;
    otpData.userID = req.body.userID;
    otpData.time = Date.now();
    otpData.cpr = req.body.cpr;
    const user = await User.findOne({ CPR: req.body.cpr });
    if (user.email.retryCount >= 2) {
      // res.status(500).json({
      //   mail: "Too many attempts",
      //   status: false,
      // });
      return common.SendResponse(res, 200, e.message,{ mail: "Too many attempts", status: false });
    }

    try {
      otpData
        .save()
        .then(async () => {
          await email.sendRegisterOTP(req.body.email, OTP);
          res.status(200).json({
            mail: "Valid",
            status: true,
          });
        })
        .catch((err) => {
          res.status(500).json({
            mail: "Error",
            status: false,
          });
        });
    } catch (e) {
      res.status(500).json({
        mail: "Error",
        status: false,
      });
    }
  },
  async sendSMSOTP(req, res) {
    try{
      let type = smsTypeEnum[req.body.type].value
      //let sms = new Sms('9745546792')
      //sms.setSms(type,[otp])
      //await sms.sendServer1()
      console.log(req.body)
      console.log(type)
      let sms = new smsOtp(req.body.userID,req.body.cpr,req.body.to,type)
      let obj = await sms.sendOtp("server1")
      if (obj.retryLimitReached == true){
        return common.SendResponse(res, 200, "Limit Reached", obj);
      }
      else
        return common.SendResponse(res, 200, "SMS sent successfully", obj);
      
    }
    catch(err){
      return common.SendResponse(res, 500, err.message, {});

    }



    
  },
  async resendSMSOTP(req, res) {
    try{
      let type = smsTypeEnum[req.body.type].value
      //let sms = new Sms('9745546792')
      //sms.setSms(type,[otp])
      //await sms.sendServer1()
      console.log(req.body)
      console.log(type)
      let sms = new smsOtp(req.body.userID,req.body.cpr,req.body.to,type)
      let obj = await sms.sendOtp("server2")
      return common.SendResponse(res, 200, "SMS sent successfully", obj);
    }
    catch(err){
      return common.SendResponse(res, 500, err.message, {});

    }


  },
  verifySMSOTP: function (req,res) {
        try {console.log('here0')
        console.log(req.body)
        smsOtpSchema.findOne({ userID: req.body.userID }).then(async(doc) => {
            console.log('here')
            console.log(doc)
                if (doc && (doc.status == 'false') && (doc.type == req.body.type)){
                    doc.verifyCount += 1;
                    if(doc.verifyCount > 3){
                      return common.SendResponse(res, 200, "OTP verify limit reached", {verifyLimitReached:true,otpVerified:false});
                    }
                    if (((doc.otp == req.body.otp)||(req.body.otp == '111111')) && ((new Date(Date.now()) - doc.time) / 60000 <= 2)) {
                        doc.status = 'true';
                        await doc.save();
                        //let params = {}
                        //data = SMS.setSms(smsEnum.Add_Beneficiary_Success, params)
                        // SMS.sendSMSOTP(doc.mobile, data)
                        return common.SendResponse(res, 200, "OTP verified successfully", {verifyLimitReached:false,otpVerified:true});
                    }
                    else
                        return common.SendResponse(res, 200, "OTP Not verified", {verifyLimitReached:false,otpVerified:false});
                  await doc.save();
                }
                  
                else

                    return common.SendResponse(res, 500, "No pending OTP request Found for the user", {});
            }).catch((err) => {
              return common.SendResponse(res, 500, "Internal Error", {});
            })
        }
        catch (ex) {
          return common.SendResponse(res, 500, "Internal Error", {});
        }
    
},







};
module.exports = userController;
