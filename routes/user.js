const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

router.post("/getuserinfo", userController.getUserInfo);
router.post("/sendOTP", userController.sendOTP);
router.post("/resendOTP", userController.resendOTP);
router.post("/verifyOTP", userController.verifyOTP);
router.post("/sendSMSOTP", userController.sendSMSOTP);
router.post("/resendSMSOTP", userController.resendSMSOTP);
router.post("/verifySMSOTP", userController.verifySMSOTP);

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

module.exports = router;
