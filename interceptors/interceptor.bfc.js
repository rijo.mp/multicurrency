const axios = require("axios");
const xml2json = require("xml2json");
const { performance } = require("perf_hooks");
const dbServices = require("../dbServices/bfcLogServices");

let userID, t1, tempRequest;

const bfcAxios = axios.create({
  baseURL: "https://bfcapis.bfccirrus.com",
});

let APIname;
const requestHandler = async (request) => {
  try {
    userID = request.params.userID;
    delete request.params.userID;
    APIname = request.params.API;
    delete request.params.API;
    t1 = performance.now();
    tempRequest = request;
    request.headers["Content-Type"] = "text/xml; charset=utf-8";
    request.headers["SOAPAction"] = `http://fossilapi.com/${APIname}`;
    return request;
  } catch (err) {
    throw new Error(err);
  }
};

const responseHandler = async (response) => {
  try {
    if (userID) {
      await dbServices.saveLog(userID, tempRequest.url.split("/").pop(), tempRequest.url, t1, tempRequest.data, response.data);
    }
    let output = xml2json.toJson(response.data);
    output = JSON.parse(output);
    output = output["s:Envelope"]["s:Body"][`${APIname}Response`][`${APIname}Result`];
    return output;
  } catch (err) {
    throw new Error(err);
  }
};

const errorHandler = async (error) => {
  if (userID) {
    await dbServices.saveErrorLog(userID, tempRequest.url.split("/").pop(), tempRequest.url, t1, tempRequest.data, error.response.data);
  }
  return Promise.reject(error);
};

bfcAxios.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);

bfcAxios.interceptors.response.use(
  (response) => responseHandler(response),
  (error) => errorHandler(error)
);

module.exports = bfcAxios;
