// const dotenv = require("dotenv").config();
const axios = require("axios");
// const { performance } = require('perf_hooks');
// const dbServices = require('../src_v031/dbServices/netsysLogs.Service');
const rs = require("jsrsasign");

let userID;
// let tempRequest,t1;
const axiosNetsys = axios.create({
  //baseURL: process.env.netsysPL,
});

function signatureGenerator(data, keyID, prvK) {
  let netsysSecretKey = prvK.split(/\\n/);
  let privateKey = "";
  for (let i = 0; i < netsysSecretKey.length; i++) {
    privateKey += netsysSecretKey[i] + "\n";
  }
  let payload = data;
  let sign = new rs.KJUR.crypto.Signature({
    alg: "SHA256withECDSA",
  });
  sign.init(privateKey);
  sign.updateString(payload);

  let signValueHex = sign.sign();
  let signBase64 = Buffer.from(signValueHex, "hex").toString("base64");
  let key = "keyId=" + keyID + ",algorithm=ecdsa-sha256,signature=" + signBase64;
  return key;
}

const requestHandler = async (request) => {
  userID = request.data.userID;
  // t1 = performance.now()
  delete request.data.userID;
  if (request.url.includes("rpc/TransactionService/Payment") && request.data.auth) {
    request.headers["signature"] = request.data.signature;
    request.headers["Authorization"] = request.data.auth;
    delete request.data.signature;
    delete request.data.auth;
    tempRequest = request;
  } else {
    const signature = await signatureGenerator(JSON.stringify(request.data), process.env.netsysDevId, process.env.bfcpk);
    request.headers["signature"] = signature;
    request.headers["Authorization"] = process.env.netsys_auth1;
    // tempRequest = request;
    return request;
  }
};

const responseHandler = (response) => {
  if (response.status === 401) {
    // window.location = '/login';
  }
  // dbServices.saveLog(userID, tempRequest.url.split('/').pop(), tempRequest.url, t1, JSON.parse(tempRequest.data), response.data);
  if (!response.data.Error) return response.data;
  else throw new Error(response.data.Error.Code);
};

const errorHandler = async (error) => {
  // await dbServices.saveErrorLog(userID, tempRequest.url.split('/').pop(), tempRequest.url, t1, JSON.parse(tempRequest.data), error.response.data);
  return Promise.reject(error);
};

axiosNetsys.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);

axiosNetsys.interceptors.response.use(
  (response) => responseHandler(response),
  (error) => errorHandler(error)
);

module.exports = axiosNetsys;
