const mongoose = require("mongoose");

let exchangeRateUpdateLogSchema = new mongoose.Schema(
  {
    status: {
      type: Boolean,
      require: true,
    },

    bfcResponse: String,
    m2pRequest: String,
    m2pResponse: String,
    timeStamp: Date,
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("exchangeRateUpdateLog", exchangeRateUpdateLogSchema);
