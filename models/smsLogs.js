const mongoose = require('mongoose');


let smsLogs = new mongoose.Schema({
    mobile: String,
    data: String,
    log: String
}, { timestamps: true })

module.exports = mongoose.model('smslogs', smsLogs)