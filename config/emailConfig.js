const dotenv = require('dotenv');
dotenv.config()

const nodemailer = require('nodemailer')
const NM = nodemailer.createTransport({
    host: process.env.mailhost,
    port: parseInt(process.env.mailport), secure: false,
    auth: {
        user: process.env.mailuser,
        pass: process.env.mailpass
    }
});

module.exports = NM;