const emailService = require('../services/service.email');

class Email {
    constructor() {

    }
    async sendRegisterOTP(toMail,otp) {  
        console.log('here');
        let  template = `
        OTP is ${otp}
        `
        let sendInfo = {
            email: toMail,
            subject:"OTP for Multicurrency card",
            html:template
        }
        await emailService.sendEmail(sendInfo)
    }
}

module.exports = Email;