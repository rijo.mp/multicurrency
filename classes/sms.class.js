const request = require("request");
const smslogs = require("../models/smsLogs")
const username = "bfcpayments";
const password = "mstMi2KG";
const username_v2 = "bfcpayments";
const password_v2 = "77157414";
const smsTypeEnum = require("../enums/smsTypeEnum")

class Sms {
    constructor(to,type){
        this.number = to
        this.data =''
        this.type = type
        this.otp = ''
    }
    //Sending the sms using the Server1
    setSms  () {
        //let text;
        switch (this.type) {
            case smsTypeEnum.addMoney: {
                this.data = `DO NOT share this code with anyone. Your OTP code for Adding Money to Travel Buddy Wallet is ${this.otp}. Please call 17711775 if not initiated by you.`
                break;
            }
        }
    }
    async sendServer1(){
        return new Promise((resolve,reject)=>{
            try{
                if(process.env.NODE_ENV == 'sandbox'){
                    console.log('here')
                    let num = "+91"
                    let number = num + this.number
                    request.post('https://api.rmlconnect.net/bulksms/bulksms?username=' + username + '&password=' + password + '&type=0&dlr=1&destination=' + number + '&source=BFCPay&message=' + this.data, async function (err, res) {
                    if (err) {
                        await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                        reject(err.message)
                    }
                    else {
                        console.log(res.body);
                        await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                        resolve(res.body);
                    }
                })
                }
                else if(process.env.NODE_ENV == 'staging'){
                    let num = "+973"
                    let toList = ['39920687', '39916101', '38320410', '32173161', '33522204', '38008679', '38011663', '38404508', '39718933', '33527323', '39642382']
                    toList.map((item) => {
                        let toNumber = num + item
                        request.post('https://api.rmlconnect.net/bulksms/bulksms?username=' + username + '&password=' + password + '&type=0&dlr=1&destination=' + toNumber + '&source=BFCPay&message=' + this.data, async function (err, res) {
                            if (err) {
                                await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                                reject(err.message)
                            }
                            else {
                                console.log(res.body);
                                await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                                resolve(res.body);
                            }
                        });
                    });
                }
                else if(process.env.NODE_ENV == 'production'){
                    let num = "+973"
                    let number = num + this.number
                    request.post('https://api.rmlconnect.net/bulksms/bulksms?username=' + username + '&password=' + password + '&type=0&dlr=1&destination=' + number + '&source=BFCPay&message=' + this.data, async function (err, res) {
                        if (err) {
                            await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                            reject(err.message)
                        }
                        else {
                            console.log(res.body);
                            await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                            resolve(res.body);
                        }
                    })
                }
        

            }
            catch(err){
                reject(err.message)
            }
        })
    }

    async sendServer2(){
        return new Promise((resolve,reject)=>{
            try{
                if(process.env.NODE_ENV == 'sandbox'){
                    let num = "+91"
                    let number = num + this.number
                    request.post('https://api.rmlconnect.net/bulksms/bulksms?username=' + username + '&password=' + password + '&type=0&dlr=1&destination=' + number + '&source=BFCPay&message=' + this.data, async function (err, res) {
                    if (err) {
                        await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                        reject(err)
                    }
                    else {
                        console.log(res.body);
                        await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                        resolve()

                    }
                })
                }
                else if(process.env.NODE_ENV == 'staging'){
                    let num = "+973"
                    let toList = ['39920687', '39916101', '38320410', '32173161', '33522204', '38008679', '38011663', '38404508', '39718933', '33527323', '39642382']
                    toList.map((item) => {
                        let toNumber = num + item
                        request.post('https://api.rmlconnect.net/bulksms/bulksms?username=' + username + '&password=' + password + '&type=0&dlr=1&destination=' + toNumber + '&source=BFCPay&message=' + this.data, async function (err, res) {
                            if (err) {
                                 await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                                reject(err.message)
                            }
                            else {
                                console.log(res.body);
                                await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                                resolve(res.body);
                            }
                        });
                    });
                }
                else if(process.env.NODE_ENV == 'production'){
                    let num = "+973"
                    let number = num + this.number
                    request.get('https://api.smscountry.com/SMSCwebservice_bulk.aspx?User=' + username_v2 + '&passwd=' + password_v2 + '&mobilenumber=' + number + '&message=' + this.data + '&sid=BFCPAY&mtype=N&DR=Y', async function (err, res) {
                        if (err) {
                            await smslogs.create({ mobile: this.number, data: this.data, log: err.message })
                            reject(err.message)
                        }
                        else {
                            console.log(res.body);
                            await smslogs.create({ mobile: this.number, data: this.data, log: JSON.stringify(res.body) })
                            resolve(res.body);
                        }
                    })
                }
        

            }
            catch(err){
                reject(err.message)
            }
        })
    }

}


module.exports = Sms;






