const Enum = require("enum");

const NetSysbaseUrl2 = process.env.netsysPL;
const KycBaseUrl = process.env.netsysKYC;
// const gpsBaseUrl = process.env.netsysGps;
const dotenv = require("dotenv").config();
// console.log(NetSysbaseUrl2, KycBaseUrl);

const netsysURLs = new Enum({
  getApplication: KycBaseUrl + "/ApplicationService/GetApplication",
  updateApplication: KycBaseUrl + "/ApplicationService/UpdateApplication",
  createApplication: KycBaseUrl + "/ApplicationService/CreateApplication",
  approveApplication: KycBaseUrl + "/ApplicationService/ApproveApplication",
  updateLedger: NetSysbaseUrl2 + "/rpc/CustomerService/UpdateCustomer",
  payment: NetSysbaseUrl2 + "/rpc/TransactionService/Payment",
  getCustomer: NetSysbaseUrl2 + "/rpc/CustomerService/GetCustomer",
  createCustomer: NetSysbaseUrl2 + "/rpc/CustomerService/AddCustomer",
  createAccount: NetSysbaseUrl2 + "/rpc/CustomerService/AddAccount",
  //   updateGPS: gpsBaseUrl + "/rpc/CardService/UpdateMobileNumber",
  updateAccountStatus: NetSysbaseUrl2 + "/rpc/AccountService/UpdateAccountStatus",
  payment: NetSysbaseUrl2 + "/rpc/TransactionService/Payment",
});

module.exports = netsysURLs;
