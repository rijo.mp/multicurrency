// const LoadMoney = require("../../../class/class.loadMoney");
const moment = require("moment-timezone");
const CardEntity = require("../classes/card.class");
const User = require("../models/userSchema");
const commonDatasSchema = require("../models/commonDatasSchema");
// const { checkAnnualSubRenewal } = require("../utils/timeUtils");
const config = require("../models/configSchema");

const { SendResponse } = require("../common/commonFunctions");

const serviceBfc = require("../services/service.bfc");
const m2pService = require("../services/services.m2p");
const exchangeRateUpdateLog = require("../models/exchangeRateUpdateLogSchema");
const transaction = require("../services/services.transactionMS");
const benefitAppModel = require("../models/benefitAppSchema");
const paymentModesModel = require("../models/paymentModesSchema");
const Joi = require("joi");

let transactionController = {
  fetchAmountSuggestions: async (req, res) => {
    try {
      let { cpr: CPR, userID } = req.body;
      const card = new CardEntity(CPR, userID);
      let { suggestionArr } = await commonDatasSchema.findOne();
      const walletBalance = await card.fetchBalance();
      let resObj = {
        BHD: walletBalance[0].balance,
        suggestionArr,
      };
      return SendResponse(res, 200, "Amount Suggestions", resObj);
    } catch (err) {
      console.log(err);
      return SendResponse(res, 500, "Internal Server Error");
    }
  },

  fetchTotalAmount: async (req, res) => {
    let card, user;
    try {
      let { cpr: CPR, userID, amount } = req.body;
      card = new CardEntity(CPR, userID);
      amount = parseFloat(amount);
      await card.checkMaxAccountLimit(amount);
      user = await User.findOne({ CPR });
      let response = {};
      if (user.isFirstAddmoney /*|| (await checkAnnualSubRenewal(user.annualSubRenewDate))*/) {
        let data = await config.find({ key: { $in: ["VAT", "AnnualSubFees"] } }).lean();
        let vat, fees;
        data.forEach((doc) => {
          if (doc.key === "VAT") vat = doc.value;
          else fees = doc.value;
        });
        response["AnnualFees"] = fees.toFixed(3);
        response[`VATPercentage`] = vat.toString();
        response[`VATAmount`] = ((fees * vat) / 100).toFixed(3);
        response["TotalFees"] = (+response["AnnualFees"] + +response["VATAmount"]).toFixed(3);
        response["TotalAmount"] = (amount + +response["TotalFees"]).toFixed(3);
        // return SendResponse(res, 200, "With Annual fee", response);
      } else {
        response["AnnualFees"] = "";
        response[`VATPercentage`] = "";
        response[`VATAmount`] = "";
        response["TotalFees"] = "";
        response["TotalAmount"] = amount.toFixed(2);
        // return SendResponse(res, 200, "Without Annual fee", response);
      }
      // await card.checkMaxAccountLimit(+response["TotalAmount"]);
      return SendResponse(res, 200, "", response);
    } catch (err) {
      console.log(err);
      return SendResponse(res, 500, card.errorMessage || "Internal Server Error");
    }
  },

  createTransaction: async (req, res) => {
    try {
      // if (await netsysUser.checkDormancy()) {
      let { amount, tax, fee, Total_amount, userID, cpr } = req.body;
      let request = {
        amountDetail: {
          amount,
          tax,
          fee,
          Total_amount,
        },
        userID,
        cpr,
        paymentType: "Add Money",
      };
      const transactionResponse = await transaction.createTransaction(request);
      const paymentModes = await paymentModesModel.find({}, { _id: 0 }).lean();
      const benefitApp = await benefitAppModel.findOne({}, { _id: 0 }).lean();
      const benefitPG = {
        type: "ends_with",
        successURL: ["success.php", "approved.php"],
        errorURL: ["error.php", "err.php"],
        webkitURL:
          process.env.addMoneyPG + "request.php?trackID=" + transactionResponse.transactionID + "&amount=" + parseFloat(Total_amount).toFixed(3),
      };
      let data = {
        paymentModes,
        benefitApp,
        benefitPG,
        transactionID: transactionResponse.transactionID,
        amountDetail: transactionResponse.amountDetail,
      };
      return SendResponse(res, "200", "Created Transaction Successfully", data);
      // } else throw new Error("Account is Dormant. Please contact customer care");
    } catch (ex) {
      console.log(ex);
      return SendResponse(res, "500", ex.message);
    }
  },

  // getPaymentMethods: async (req, res) => {
  //   try {
  //     const paymentModes = await paymentModesModel.find({}, { _id: 0 }).lean();
  //     const benefitApp = await benefitAppModel.find({}, { _id: 0 }).lean();
  //     const benefitPG = {
  //       type: urlTypeEnum.addMoneyURL.value,
  //       successURL: ["success.php", "approved.php"],
  //       errorURL: ["error.php", "err.php"],
  //       webkitURL:
  //         process.env.addMoneyPG + "request.php?trackID=" + createResponse.transactionID + "&amount=" + parseFloat(fee + cardFee.vatFee).toFixed(3),
  //     };
  //     const response = {
  //       paymentModes,
  //       benefitApp,
  //       benefitPG,
  //     };
  //     return SendResponse(res, "200", "Payment Modes", response);
  //   } catch (err) {
  //     console.log(ex);
  //     return SendResponse(res, "500", err.message);
  //   }
  // },
  updateTransaction: async (req, res) => {
    let payload,
      transactionStatus = false,
      CPR = req.body.cpr,
      transactionResponse,
      amountDetail,
      time,
      transactionID = req.body.transactionID,
      m2pLog,
      m2pStatus,
      m2pTransactionID,
      resObj;
    try {
      const { status, paymentMode, PGReferenceID } = req.body;
      transactionResponse = await transaction.updateTransaction({ status, transactionID, paymentMode, PGReferenceID });
      amountDetail = transactionResponse.amountDetail;
      time = moment(transactionResponse.updateTime).tz("Asia/Bahrain").format("YYYY-MM-DD hh:mm A");
      resObj = {
        amountDetail,
        time,
        transactionID,
      };
      if (transactionResponse.status) {
        payload = {
          userID: req.body.userID,
          amount: transactionResponse.amountDetail.Total_amount,
          toEntityId: CPR,
          transactionOrigin: paymentMode === "BENEFITPG" ? "WEB" : "MOBILE",
          externalTransactionId: transactionID,
          businessEntityId: "BFCFX",
          businessType: "BFCFX",
          description: "LOAD MONEY",
          fromEntityId: "BFCFX01",
          fromProductId: "BHD",
          productId: "BHD",
          transactionType: "M2C",
          yapcode: "1234",
          fxSettlementType: "FX",
        };
        // console.log(payload);
        let data = await m2pService.addMoney(payload);
        if (data.result) {
          [m2pLog, m2pStatus, m2pTransactionID] = [JSON.stringify({ request: payload, response: data }), true, `${data.result.txId}`];
          transactionStatus = true;
          return SendResponse(res, "200", "Transaction Success", { ...resObj, transactionStatus });
        }
      } else {
        return SendResponse(res, "200", "Transaction Failed", { ...resObj, transactionStatus });
      }
    } catch (err) {
      console.log(err.exception?.shortMessage || err.message || err);
      if (err.exception) {
        [m2pLog, m2pStatus, m2pTransactionID] = [JSON.stringify({ request: payload, response: err }), false, "NIL"];
        return SendResponse(res, "200", err.exception?.shortMessage || "Internal Server Error", { ...resObj, transactionStatus });
      }
      return SendResponse(res, "500", err.exception?.shortMessage || err.message || "Internal Server Error");
    } finally {
      try {
        transactionResponse = await transaction.updateTransaction({ transactionID, m2pLog, m2pStatus, m2pTransactionID });
        if (transactionStatus) {
          let user = await User.findOne({ CPR });
          if (user.isFirstAddmoney) {
            user.isFirstAddmoney = false;
            user.save();
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  },

  async uploadExchangeRateCron(req, res) {
    let [status, bfcResponse, m2pRequest, m2pResponse] = [false, "", [], ""];
    try {
      bfcResponse = await serviceBfc.getExchangeRateByRSCODE();
      for (let curObj of bfcResponse.ExchangeRates) {
        let payload = {
          sourceCurrency: curObj.CurrencyCode,
          destinationCurrency: "BHD",
          conversionRate: curObj.BuyCashRate,
          changer: "BFC@m2p.in",
        };
        m2pRequest.push(payload);
      }
      m2pResponse = await m2pService.updateConversionRate(m2pRequest);
      status = true;
      return SendResponse(res, 200, "Rates Updated", m2pRequest);
    } catch (ex) {
      console.log(ex);
      return SendResponse(res, 500, "Internal Server Error");
    } finally {
      try {
        let log = new exchangeRateUpdateLog();
        log.status = status;
        log.bfcResponse = JSON.stringify(bfcResponse);
        log.m2pRequest = JSON.stringify(m2pRequest);
        log.m2pResponse = JSON.stringify(m2pResponse);
        log.timeStamp = new Date();
        await log.save();
      } catch (err) {
        console.log(err.message);
      }
    }
  },

  w2wTransaction: async (req, res) => {
    const w2wTransactionSchema = Joi.object({
      fromEntityId: Joi.string().required(),
      amount: Joi.number().required(),
      productId: Joi.string().required(),
      business: Joi.string().required(),
      description: Joi.string().required(),
      sourceCurrency: Joi.string().required(),
      transactionOrigin: Joi.string(),
      transactionType: Joi.string(),
      destinationCurrency: Joi.string(),
      toEntityId: Joi.string(),
      externalTransactionId: Joi.string(),
      businessType: Joi.string(),
      fromProductId: Joi.string(),
    });
    // "fromEntityId": "BCIPM9322E",
    // "amount": "2",
    // "productId": "USD",
    // "business": "BFCFX",
    // "description": "Wallet To Wallet Transfer",
    // "sourceCurrency": "USD",
    // "transactionOrigin": "WEB",
    // "transactionType": "W2W_FOREX",
    // "destinationCurrency": "EUR",
    // "toEntityId": "",
    // "externalTransactionId": "889d5768-1d65fdvc-41fc-9c2d-fcf71354aa6f",
    // "businessType": "BFCFX",
    // "fromProductId": "USD"

    let request = {
      amountDetail: {
        amount: req.body.amount,
        tax: 0,
        fee,
        Total_amount,
      },
      userID,
      cpr,
      paymentType: "Add Money",
    };
    let transactionResponse = await transaction.createTransaction(request);
    let data = {
      transactionID: transactionResponse.transactionID,
      amountDetail: transactionResponse.amountDetail,
    };

    let payload = {
      fromEntityId: req.body.entityId,
      amount: req.body.amount,
      productId: req.body.productId,
      business: process.env.business,
      description: "Wallet to Wallet Transfer",
      sourceCurrency: req.body.sourceCurrency,
      transactionOrigin: "WEB",
      transactionType: "W2W_FOREX",
      destinationCurrency: req.body.destinationCurrency,
      toEntityId: "",
      externalTransactionId: externalTransactionId,
      businessType: process.env.businessType,
      fromProductId: "",
    };
  },
};

module.exports = transactionController;
