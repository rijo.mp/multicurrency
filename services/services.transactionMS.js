const axiosTransactionMS = require("../interceptors/interceptor.transactionMS");

let transactionService = {
  createTransaction(request) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosTransactionMS.post("/paymentMicroServices/multicurrency/createTransaction", request);
        if (data.status) resolve(data);
        else throw new Error("Failed to register Transaction");
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  },
  updateTransaction(request) {
    return new Promise(async (resolve, reject) => {
      try {
        let data = await axiosTransactionMS.post("/paymentMicroServices/multicurrency/updateTransaction", request);
        resolve(data);
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  },
};
module.exports = transactionService;
