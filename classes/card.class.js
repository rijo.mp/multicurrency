const m2pService = require("../services/services.m2p");
const Config = require("../models/configSchema");
const cardServices = require("../dbServices/cardServices");
const { fetchBase64Image, generateToken } = require("../utils/cardUtils");
const Cards = require("../models/cardSchema");
const User = require("../models/userSchema");
const KitNumber = require("../models/kitNumberSchema");
const Application = require("../models/applicationDataSchema");
const serviceBfc = require("../services/service.bfc");

class Card {
  constructor(cpr, userID) {
    this.entityId = cpr;
    this.userID = userID;
    this.error = false;
    this.errorMessage = "";
    this.data = null;
  }

  // async getDBUserAndCard(){
  //   return new Promise(async (resolve, reject) => {
  //     try{
  //       this.userDoc=await User.findOne({CPR:this.entityId})
  //       this.cardDoc=await Card.findOne({CPR:this.entityId})
  //       resolve()
  //     }catch(err){
  //       console.log(err)
  //       reject(err)
  //     }
  //   })
  // }
  async fetchBalance() {
    return new Promise(async (resolve, reject) => {
      try {
        const data = await m2pService.fetchBalance(this.entityId, this.userID);
        // let currencies = ["USD", "EUR", "AED", "GBP", "SAR", "BHD"];
        let { value: currencies1 } = await Config.findOne({ key: "currencies" });
        console.log(currencies1);
        let walletBalance = [];
        for (let cur of currencies1) {
          let curObj = {
            currency: cur,
            balance: data.result[cur],
            //flag: await fetchBase64Image(`${__dirname}/../assets/flags/${cur.toLowerCase()}.png`),
            flag: `https://s3.me-south-1.amazonaws.com/cxportal.bfcpay.com/flags/${cur.toLowerCase()}.png`,
          };
          walletBalance.push(curObj);
        }
        resolve(walletBalance);
        //return SendResponse(res, 200, "", walletBalance);
      } catch (err) {
        console.log(err.message);
        // return SendResponse(res, 500, "Internal Server Error");
        this.error = true;
        this.errorMessage = err.exception?.shortMessage || "Internal Error";
        reject(err);
      }
    });
  }
  async fetchURLs() {
    return new Promise(async (resolve, reject) => {
      try {
        let { value: urls } = await Config.findOne({ key: "urls" }, { _id: 0 }).lean();
        resolve(urls);
      } catch (err) {
        reject(err);
      }
    });
  }
  async fetchAndUpdateCardStatus(userData, cardData) {
    return new Promise(async (resolve, reject) => {
      try {
        // let { value: urls } = await Config.findOne({ key: "urls" }, { _id: 0 }).lean();
        // resolve(urls);
        await cardServices.updateCarddataInDB(this.entityId, this.userID, userData, cardData);
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  }
  async fetchPreference() {
    let cardOps = ["ATM", "POS", "ECOM", "CONTACTLESS"];
    return new Promise(async (resolve, reject) => {
      let payload = {
        entityId: this.entityId,
        userID: this.userID,
      };
      console.log(payload);
      try {
        let resp = await m2pService.fetchPreference(payload);
        if (resp.result == null) {
          throw new Error(resp.exception ? resp.exception.shortMessage : "Internal Error");
        } else {
          let obj = {
            ATM: resp.result.atm == true ? "ALLOWED" : "NOTALLOWED",
            POS: resp.result.pos == true ? "ALLOWED" : "NOTALLOWED",
            ECOM: resp.result.ecom == true ? "ALLOWED" : "NOTALLOWED",
            CONTACTLESS: resp.result.contactless == true ? "ALLOWED" : "NOTALLOWED",
          };
          let objt = [];
          console.log(resp.result);
          for (const [key, value] of Object.entries(resp.result)) {
            console.log(`${key}: ${value}`);
            cardOps.includes(key.toUpperCase()) ? objt.push({ service: key.toUpperCase(), value: value ? "ALLOWED" : "NOTALLOWED" }) : "";
          }
          console.log(objt);
          this.data = { services: objt };
          resolve();
        }
      } catch (e) {
        this.error = true;
        this.errorMessage = e.message;
        reject();
      }
    });
  }
  async setPreference(type, status) {
    return new Promise(async (resolve, reject) => {
      let payload = {
        entityId: this.entityId,
        status: status,
        type: type,
        userID: this.userID,
      };
      console.log(payload);
      try {
        let resp = await m2pService.setPreference(payload);
        console.log(123);
        // console.log(resp)
        if (resp.result == null) {
          this.error = true;
          this.errorMessage = resp.exception.shortMessage;
          reject();
        } else {
          this.data = resp.result;
          resolve();
        }
      } catch (e) {
        this.error = true;
        this.errorMessage = "M2P Error";
        reject();
      }
    });
  }

  async changeCardStatus(flag, kitNo) {
    return new Promise(async (resolve, reject) => {
      try {
        let payload = {
          entityId: this.entityId,
          flag,
          kitNo,
          reason: "Done by Customer",
          userID: this.userID,
        };
        console.log(payload);
        let resp = await m2pService.changeCardStatus(payload);
        // console.log(123);
        console.log(resp);
        this.data = resp.result;
        resolve();
      } catch (err) {
        this.error = true;
        this.errorMessage = err.exception?.shortMessage || "Internal Server Error";
        reject(err);
      }
    });
  }

  async getPCIWidgetUrl(type, userDoc, cardDoc) {
    return new Promise(async (resolve, reject) => {
      try {
        let token,
          // kitNo = cardDoc.cards[cardDoc.cards.length - 1].kitNo,
          [card] = cardDoc.cards.filter((card) => card.cardStatus != "REPLACED"),
          entityId = this.entityId,
          dob = userDoc.DOB.split("-").reverse().join("");
        token = await generateToken(card.kitNo, entityId, dob);
        let payload = {
          type,
          userID: this.userID,
          token,
          kitNo: card.kitNo,
          entityId,
          appGuid: "123dase",
          business: "TECHSUPPORT01",
          callbackUrl: "https://www.google.com",
          dob,
        };
        const resp = await m2pService.getPCIWidgetUrl(payload);
        this.data = { url: resp.result, callbackUrl: payload.callbackUrl };
        resolve();
      } catch (err) {
        this.error = true;
        this.errorMessage = err.exception?.shortMessage || "Internal Server Error";
        reject(err);
      }
    });
  }
  async updateCustomer(data) {
    return new Promise(async (resolve, reject) => {
      try {
        let updateresp = await m2pService.updateCustomer(data);
        let user = await User.findOne({ entityId: this.entityId });
        // let emailAddress = data.emailAddress
        // let contactNo = data.contactNo;
        // let address = data.addressDto.address;
        let updateResp = await m2pService.updateCustomer(data);
        user.emailAddress = data.emailAddress;
        user.contactNo = data.contactNo;
        user.address = data.addressDto.address;
        await user.save();
        console.log(updateresp);
        resolve(updateresp);
      } catch (err) {
        reject(err);
      }
    });
  }
  async replaceCard(address) {
    return new Promise(async (resolve, reject) => {
      let user, card;
      console.log("Rep");
      let newKitStatus = false;
      let replaceStatus = false;
      let newKitNumber = null;
      try {
        if (address == null || "") {
          this.error = true;
          this.errorMessage = "Address cannot be null";
          //console.log(err.message)
          throw new Error(this.errorMessage);
        }

        user = await User.findOne({ entityId: this.entityId });
        console.log(user);
        let card = await Cards.findOne({ entityId: this.entityId });
        console.log(card);
        console.log("#################" + this.userID);
        //if(user.status != "REPLACED")
        user.status = "BLOCKED";
        await user.save();

        // chnage address
        let application = await Application.findOne({ entityId: this.entityId });
        let addresses = application.addressInfo;
        addresses = addresses.filter((ad) => {
          return ad.addressCategory != "DELIVERY";
        });
        addresses.push(address);
        let updateCustomerPayload = {
          entityId: this.entityId,
          emailAddress: application.communicationInfo[0].emailId,
          contactNo: application.communicationInfo[0].contactNo,
          addressDto: {
            contactNo2: application.communicationInfo[0].contactNo,
            address: addresses,
          },
          userID: this.userID,
        };
        await this.updateCustomer(updateCustomerPayload);
        let activecard = card.cards.filter((item) => {
          return item.cardStatus != "REPLACED";
        });
        let oldKitNumber = activecard[0].kitNo;
        console.log("oldKitNumber" + oldKitNumber);
        newKitNumber = await KitNumber.findOneAndUpdate(
          { status: false, isInvalid: false },
          { status: true, entityId: this.entityId },
          { returnOriginal: false }
        );
        newKitStatus = true;
        if (!newKitNumber) throw new Error("No available kit number");

        let payload = {
          entityId: this.entityId,
          flag: "BL",
          kitNo: oldKitNumber,
          reason: "Replace requested by the customer",
          userID: this.userID,
        };
        console.log(payload);
        let respCardStatus;
        try {
          respCardStatus = await m2pService.changeCardStatus(payload);
          console.log("block###########################");
        } catch (e) {
          if (e.exception.shortMessage != "Card already blocked") {
            throw new Error("Internal Server Error: ");
          }
        }

        // console.log(123);
        // console.log(respCardStatus);
        // this.data = respCardStatus.result;
        // console.log(this.data);
        try {
          let replacePayload = {
            entityId: this.entityId,
            oldKitNo: oldKitNumber,
            newKitNo: newKitNumber.kitNo,
            userID: this.userID,
          };
          console.log(replacePayload);
          let respReplaceCard = await m2pService.replaceCard(replacePayload);
          console.log("replaceCard");
          replaceStatus = true;
          this.data = "Replaced";
          console.log(replaceStatus + "84739477384934983434834983498374983498");
          resolve();
        } catch (err) {
          this.error = true;
          this.errorMessage = err.exception?.shortMessage || "Internal Server Error";
          if (err.exception && err.exception.errorCode == "Y3102") {
            newKitNumber.isInvalid = true;
          }
          console.log(err.message);
          reject(err);
        }
      } catch (err) {
        this.error = true;
        this.errorMessage = err.exception?.shortMessage || err.message || "Internal Server Error";
        console.log(err.message);
        reject();
      } finally {
        if (newKitStatus && replaceStatus) {
          newKitNumber.status = true;
          await newKitNumber.save();
        } else if (newKitStatus) {
          newKitNumber.entityId = "";
          await newKitNumber.save();
        }
        if (user != null) {
          let card = await Cards.findOne({ entityId: this.entityId });
          await this.fetchAndUpdateCardStatus(user, card);
        }
      }
    });
  }

  async checkMaxAccountLimit(amount) {
    return new Promise(async (resolve, reject) => {
      try {
        let walletData = await this.fetchBalance();
        let exchangeRateData = await serviceBfc.getExchangeRateByRSCODE(this.userID);
        let totalAmountBhd = 0;
        for (let walletObj of walletData) {
          for (let exchangeRateObj of exchangeRateData.ExchangeRates) {
            if (walletObj.currency === exchangeRateObj.CurrencyCode) {
              totalAmountBhd += walletObj.balance * exchangeRateObj.BuyCashRate;
              break;
            }
          }
        }
        if (Number((totalAmountBhd + amount).toFixed(5)) > 2500) {
          throw new Error(`Maximum account limit crossed. Max amount that can be deposited is ${(2500 - totalAmountBhd).toFixed(2)}`);
        } else {
          resolve();
        }
      } catch (err) {
        this.error = true;
        this.errorMessage = err.exception?.shortMessage || err.message || "Internal Server Error";
        reject(err);
      }
    });
  }
}

module.exports = Card;
