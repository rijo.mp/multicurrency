const mongoose = require("mongoose");

const paymentModesSchema = new mongoose.Schema(
  {
    ID: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    settlementCode: {
      type: String,
      required: true,
    },
    icon: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    sms: {
      type: Boolean,
      required: true,
    },
    status: {
      type: Boolean,
      required: true,
    },
    url: String,
  },
  { versionKey: false }
);

module.exports = mongoose.model("paymentModes", paymentModesSchema);
