const Enum = require("enum");

const addressEnum = new Enum({
  AddressType: "addressCategory",
  BuildingAlpha: "AHOname",
  FlatNumber: "AHOnumber",
  FloorNumber: "floorNo",
  BuildingNumber: "buildingNo",
  RoadNumber: "roadNo",
  BlockNumber: "blockNo",
});

module.exports = addressEnum;
