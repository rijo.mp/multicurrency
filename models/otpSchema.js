const mongoose      =   require('mongoose');

let otpSchema   = new mongoose.Schema({
    otp     :   String,
    email  :   String,
    status  :   String,
    time    :   Date,
    userID  :   String,
    cpr     :   String
},{
    versionKey  : false
})

module.exports = mongoose.model('OtpSchema',otpSchema);