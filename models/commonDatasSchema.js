const mongoose = require("mongoose");

const cashLimitSchema = new mongoose.Schema(
  {
    name: String,
    value: String,
  },
  { _id: false }
);

const commonDataSchema = new mongoose.Schema(
  {
    additionalChargesURL: String,
    cashLimit: [cashLimitSchema],
    termsAndConditionsURL: String,
    maxRetries: Number,
    // kitNoArray: [Number],
    suggestionArr: [String],
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("commonData", commonDataSchema);
6900000042;
