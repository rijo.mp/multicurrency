const bfcLog = require("../models/bfcRequestResponse");
const { performance } = require("perf_hooks");

let bfcDbServices = {
  saveLog(userID, name, url, t1, request, response) {
    return new Promise(async (resolve, reject) => {
      try {
        let t2 = performance.now();
        let temp = new bfcLog();
        temp.userID = userID;
        temp.name = name;
        temp.url = url;
        temp.timeRequired = t2 - t1;
        temp.request = request;
        temp.timeStamp = new Date(Date.now());
        temp.response = response;
        await temp.save();
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  },
  saveErrorLog(userID, name, url, t1, request, error) {
    return new Promise(async (resolve, reject) => {
      try {
        let t2 = performance.now();
        let temp = new bfcLog();
        temp.userID = userID;
        temp.name = name;
        temp.url = url;
        temp.timeRequired = t2 - t1;
        temp.request = request;
        temp.timeStamp = new Date(Date.now());
        temp.response = error;
        await temp.save();
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  },
};

module.exports = bfcDbServices;
