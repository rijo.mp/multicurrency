const mongoose = require("mongoose");
let configSchema   = new mongoose.Schema({
    key: String,
    value: mongoose.Schema.Types.Mixed
},{
    versionKey  : false,
})

module.exports = mongoose.model('configs',configSchema);