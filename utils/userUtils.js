module.exports.getNameList = function (firstname, lastname) {
  const words = `${firstname} ${lastname}`.split(" ").filter((word) => word != "");
  const arr = [];
  for (let i in words) {
    let subArr = [];
    i == 0
      ? subArr.push(...(words[i].length === 1 ? [words[i]] : [words[i], words[i][0]]))
      : subArr.push(...(words[i].length === 1 ? [` ${words[i]}`] : [` ${words[i]}`, ` ${words[i][0]}`]));
    arr.push(subArr);
  }
  function combine() {
    return processArrays([].slice.call(arguments), "", []);
    function processArrays(arrays, str, res) {
      if (arrays.length === 0) res.push(str);
      else for (let i = 0; i < arrays[0].length; i++) processArrays(arrays.slice(1), str + arrays[0][i], res);
      return res;
    }
  }
  return combine(...arr).slice(0, -1);
};

module.exports.validatePhNo = function (phoneNumber) {
  return phoneNumber.trim().startsWith("+973") ? phoneNumber.trim() : `+973${phoneNumber.trim()}`;
};
