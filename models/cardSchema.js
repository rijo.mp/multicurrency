const mongoose = require("mongoose");

const kitSchema = new mongoose.Schema(
  {
    cardNumber: String,
    kitNo: {
      type: String,
      required: true,
    },
    expiryDate: String,
    cardStatus: String,
    cardType: String,
    networkType: String,
  },
  { _id: false, timestamps: true }
);

const cardSchema = new mongoose.Schema(
  {
    entityId: {
      type: String,
      required: true,
    },
    CPR: {
      type: String,
      required: true,
    },
    userID: {
      type: String,
      // required: true,
    },
    cards: [kitSchema],
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("card", cardSchema);
