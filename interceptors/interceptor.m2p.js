const axios = require("axios");
const { performance } = require("perf_hooks");
const dbServices = require("../dbServices/m2pLogServices");

let userID, t1, tempRequest;

const m2pAxios = axios.create({
  baseURL: "https://sit-secure.yappay.in/Yappay",
});

const requestHandler = async (request) => {
  if (!request.url.includes("forex-manager/updateConversionRate")) {
    if (request.method === "get") {
      userID = request.params.userID;
      delete request.params.userID;
    } else {
      userID = request.data.userID;
      delete request.data.userID;
    }
  }
  t1 = performance.now();
  tempRequest = request;
  request.headers.Authorization = "Basic YWRtaW46YWRtaW4=";
  request.headers.TENANT = request.url.includes("txn-manager/create") ? "M2P" : "BFCFX";
  return request;
};

const responseHandler = async (response) => {
  if (response.status === 401) {
    // window.location = '/login';
  }
  // if (response?.data?.exception?.errorCode) {
  //   throw new Error(response);
  // }
  if (!tempRequest.url.includes("forex-manager/updateConversionRate")) {
    if (response.config.method === "get") {
      await dbServices.saveLog(userID, tempRequest.url.split("/")[1], tempRequest.url, t1, response.config.params, response.data);
    } else {
      await dbServices.saveLog(userID, tempRequest.url.split("/").pop(), tempRequest.url, t1, JSON.parse(tempRequest.data), response.data);
    }
  }
  return response.data;
};

const errorHandler = async (error) => {
  //write log to db
  if (!tempRequest.url.includes("forex-manager/updateConversionRate")) {
    if (error.response.config.method === "get") {
      await dbServices.saveLog(userID, tempRequest.url.split("/")[1], tempRequest.url, t1, error.response.config.params, error.response.data);
    } else {
      await dbServices.saveErrorLog(userID, tempRequest.url.split("/").pop(), tempRequest.url, t1, JSON.parse(tempRequest.data), error.response.data);
    }
  }
  console.log(error.response.data.exception);
  return Promise.reject(error.response.data);
};

m2pAxios.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);

m2pAxios.interceptors.response.use(
  (response) => responseHandler(response),
  (error) => errorHandler(error)
);

module.exports = m2pAxios;
