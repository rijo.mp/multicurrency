const mongoose      =   require('mongoose');

let smsOtpSchema   = new mongoose.Schema({
    otp     :   String,
    mobile  :   Number,
    status  :   String,
    time    :   Date,
    userID  :   String,
    cpr     :   String,
    type    :   String,
    retryCount: Number,
    verifyCount: Number
},{
    versionKey  : false
})

module.exports = mongoose.model('smsotps',smsOtpSchema);