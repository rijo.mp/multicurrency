const mongoose = require("mongoose");

let benefitAppSchema = new mongoose.Schema(
  {
    type: String,
    appId: String,
    merchantId: String,
    secret: String,
    country: String,
    currency: String,
    merchantCategoryCode: String,
    merchantName: String,
    merchantCity: String,
    url: String,
  },
  { versionKey: false }
);

module.exports = mongoose.model("benefitApp", benefitAppSchema);
