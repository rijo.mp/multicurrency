const mongoose = require("mongoose");

const applicationStatus = new mongoose.Schema(
  {
    status: Boolean,
    retryCount: {
      type: Number,
      default: 0,
    },
    m2pResponse: String,
    time: { type: Date, default: Date.now() },
  },
  { _id: false }
);

const addressSchema = new mongoose.Schema(
  {
    addressCategory: String,
    address1: String,
    address2: String,
    address3: String,
    city: String,
    state: String,
    country: String,
    pincode: String,
  },
  { _id: false }
);
// const kitSchema = new mongoose.Schema(
//   {
//     kitNo: {
//       type: String,
//       required: true,
//     },
//     cardType: String,
//     cardCategory: String,
//     cardRegStatus: String,
//     aliasName: String,
//   },
//   { _id: false }
// );

const communicationInfoSchema = new mongoose.Schema(
  {
    contactNo: String,
    emailId: String,
  },
  { _id: false }
);
const dateInfoSchema = new mongoose.Schema(
  {
    dateType: String,
    date: String,
  },
  { _id: false }
);

const ApplicationDataSchema = new mongoose.Schema(
  {
    entityId: {
      type: String,
      // required: true,
    },
    userID: {
      type: String,
      required: true,
      // required: true,
    },
    businessId: {
      type: String,
      // required: true,
    },
    title: {
      type: String,
      // required: true,
    },
    firstName: {
      type: String,
      // required: true,
    },
    lastName: {
      type: String,
      // required: true,
    },
    nameOnCard: String,
    gender: {
      type: String,
      // required: true,
    },
    // kitInfo: [kitSchema],
    addressInfo: [addressSchema],
    communicationInfo: [communicationInfoSchema],
    dateInfo: [dateInfoSchema],
    applicationStatus: applicationStatus,
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("ApplicationData", ApplicationDataSchema);
