const User = require("../models/userSchema");
const Card = require("../models/cardSchema");

const m2pService = require("../services/services.m2p");
const netsysService = require("../services/services.netsys");
const CommonDatas = require("../models/commonDatasSchema");
const kitNumbers = require("../models/kitNumberSchema");
const applicationDatasModel = require("../models/applicationDataSchema");
const { getNameList, validatePhNo } = require("../utils/userUtils");
const { SendResponse } = require("../common/commonFunctions");
const transactionService = require("../services/services.transactionMS");
const { convertDateFormat } = require("../utils/timeUtils");
const cardEntity = require("../classes/card.class");

const { convertCardDetailsFormat, convertAddressFormat, fetchBase64Image } = require("../utils/cardUtils");
const cardServices = require("../dbServices/cardServices");

let cardController = {
  async getMulticurrencyCardDetails(req, res) {
    let responseUserObj;
    try {
      req.body.userID = req.body.userID || req.body.cpr;
      let user = await User.findOne({ CPR: req.body.cpr });
      if (!user) {
        const netsysCustomer = await netsysService.getCustomer(req.body.cpr);
        const netsysApplication = await netsysService.getApplication(req.body.cpr);
        // console.log(netsysApplication[0].Address);
        // console.log(netsysCustomer.address);
        const userObject = {
          entityId: req.body.cpr,
          CPR: req.body.cpr,
          userID: req.body.userID,
          title: netsysCustomer.title || "",
          firstName: netsysCustomer.firstName || "",
          lastName: netsysCustomer.lastName || "",
          nameOnCard: `${netsysCustomer.firstName || ""} ${netsysCustomer.lastName || ""}`,
          gender: netsysCustomer.gender || "OTHER",
          DOB: (await convertDateFormat(netsysCustomer.DOB)) || "",
          addressInfo: [convertAddressFormat(netsysApplication[0].Address)],
          emailInfo: { ID: netsysCustomer.contact.email || "" },
          phoneNo: validatePhNo(netsysCustomer.contact.phoneNumber),
        };
        user = await User.create(userObject);
      }

      let card = await Card.findOne({ CPR: req.body.cpr });
      if (!card) {
        card = await Card.create({
          entityId: req.body.cpr,
          CPR: req.body.cpr,
          userID: req.body.userID,
          cards: [],
        });
      }

      const { cashLimit, termsAndConditionsURL, additionalChargesURL } = await CommonDatas.findOne();
      responseUserObj = {
        firstName: user.firstName,
        lastName: user.lastName,
        nameOnCard: user.nameOnCard || "",
        nameList: getNameList(user.firstName, user.lastName),
        email: user.emailInfo,
        address: user.addressInfo,
        cashLimit,
        additionalChargesURL,
        termsAndConditionsURL,
      };

      // const data = await m2pService.getCardList(req.body.cpr, user.userID);
      // const cardsArray = convertCardDetailsFormat(data.result);
      // user.status = cardsArray[0].cardStatus;
      // await user.save();
      // card.cards = cardsArray;
      // await card.save();
      await cardServices.updateCarddataInDB(req.body.cpr, user.userID, user, card);
      const resObj = {
        ...responseUserObj,
        status: user.status,
        cards: card.cards,
      };
      return SendResponse(res, 200, "", resObj);
    } catch (err) {
      if (err.exception?.errorCode === "Y109") {
        //"Not a registered Customer"
        return SendResponse(res, 200, "NO CARD", {
          ...responseUserObj,
          status: "NO CARD",
          cards: [],
        });
      } else {
        console.log(err.message);
        return SendResponse(res, 500, "Internal Server Error");
      }
    }
  },

  async applyCard(req, res) {
    let kitNo,
      applicationData,
      user,
      registered = false;
    try {
      user = await User.findOneAndUpdate(
        { CPR: req.body.cpr },
        { addressInfo: req.body.addressInfo, nameOnCard: req.body.nameOnCard },
        { returnOriginal: false }
      );

      const applicationDataObject = {
        entityId: req.body.cpr,
        userID: user.userID,
        businessId: req.body.cpr,
        title: user.title,
        firstName: user.nameOnCard?.split(" ")[0] || user.firstName,
        lastName: user.nameOnCard?.split(" ").slice(1).join(" ") || user.lastName,
        gender: user.gender,
        addressInfo: [
          {
            addressCategory: "DELIVERY",
            address1: user.addressInfo?.[0].AHOname,
            address2: user.addressInfo?.[0].AHOnumber || "",
            address3: null,
            city: user.addressInfo?.[0].floorNo || "",
            state: user.addressInfo?.[0].buildingNo || "",
            country: user.addressInfo?.[0].roadNo || "",
            pincode: user.addressInfo?.[0].blockNo || "",
          },
        ],
        communicationInfo: [
          {
            contactNo: user.phoneNo || "",
            emailId: user.emailInfo?.ID || "",
          },
        ],
        dateInfo: [
          {
            dateType: "DOB",
            date: user.DOB,
          },
        ],
      };

      applicationData = await applicationDatasModel.findOne({ entityId: req.body.cpr });
      if (!applicationData) {
        applicationData = await applicationDatasModel.create({
          ...applicationDataObject,
          applicationStatus: { status: false },
        });
      } else {
        applicationData = await applicationDatasModel.findOneAndUpdate(
          { entityId: req.body.cpr },
          {
            ...applicationDataObject,
            applicationStatus: {
              status: false,
              retryCount: applicationData.applicationStatus.retryCount + 1,
              time: Date.now(),
            },
          },
          { returnOriginal: false }
        );
      }

      kitNo = await kitNumbers.findOneAndUpdate(
        { status: false, isInvalid: false },
        { status: true, entityId: req.body.cpr },
        { returnOriginal: false }
      );
      if (!kitNo) return SendResponse(res, 500, "No Kit Numbers Available");
      // console.log(kitNo.kitNo);

      const m2pRequest = {
        ...applicationDataObject,
        entityType: "CUSTOMER",
        businessType: "BFCFX",
        channelName: "MULTI_CURRENCY_NO_WAIVER",
        kitInfo: [
          {
            kitNo: `${kitNo.kitNo}`,
            cardType: "PHYSICAL",
            cardCategory: "PREPAID",
            cardRegStatus: "ACTIVE",
            aliasName: user.nameOnCard || "",
          },
        ],
        kycInfo: [
          {
            documentType: "CPR",
            documentNo: req.body.cpr,
          },
        ],
      };
      // throw new Error("TEST_ERROR");
      // let transactionData = await transactionService.createTransaction("0", user.userID);
      // console.log(transactionData.transactionID);
      const m2pResponse = await m2pService.register(m2pRequest);
      if (m2pResponse?.result?.kitNo === m2pRequest.kitInfo[0].kitNo) {
        // transactionData = await transactionService.updateTransaction(true, transactionData.transactionID);
        applicationData.applicationStatus.status = true;
        applicationData.applicationStatus.m2pResponse = JSON.stringify(m2pResponse.result);
        applicationData.applicationStatus.date = Date.now();
        await applicationData.save();
        registered = true;
        return SendResponse(res, 200, "CUSTOMER REGISTERED", {
          // transactionId: transactionData.transactionID,
          transactionId: "25345345634",
          registered,
          entityId: m2pResponse.result.entityId,
          kitNo: m2pResponse.result.kitNo,
        });
      }
    } catch (err) {
      console.log(err.message);
      if (err.exception?.errorCode === "Y503") {
        applicationData.applicationStatus.status = true;
        applicationData.applicationStatus.date = Date.now();
        applicationData.applicationStatus.m2pResponse = JSON.stringify(err.exception);
        await applicationData.save();

        return SendResponse(res, 500, "Customer Already Registered");
      }
      if (err.exception?.errorCode === "Y2272") {
        applicationData.applicationStatus.date = Date.now();
        applicationData.applicationStatus.m2pResponse = JSON.stringify(err.exception);
        await applicationData.save();
        kitNo.isInvalid = true;
        await kitNo.save();

        return SendResponse(res, 500, "Invalid Kit Number");
      }
      return SendResponse(res, 500, "Internal Server Error");
    } finally {
      try {
        if (!registered && kitNo) {
          kitNo.status = false;
          kitNo.entityId = "";
          await kitNo.save();
        }
        if (registered && kitNo) {
          let card = await Card.findOne({ CPR: req.body.cpr });
          // if (!card) {
          //   card = await Card.create({
          //     entityId: req.body.cpr,
          //     CPR: req.body.cpr,
          //     userID: user.userID,
          //     cards: [],
          //   });
          // }
          // const data = await m2pService.getCardList(req.body.cpr, user.userID);
          // const cardsArray = convertCardDetailsFormat(data.result);
          // user.status = cardsArray[0].cardStatus;
          // await user.save();
          // card.cards = cardsArray;
          // await card.save();
          await cardServices.updateCarddataInDB(req.body.cpr, user.userID, user, card);
        }
      } catch (err) {
        console.log(err);
      }
    }
  },
  // async getDashboardData(req, res) {
  //   try {
  //     let card = new cardEntity(req.body.cpr, req.body.userID);
  //     card.fetchBalance();
  //     const data = await m2pService.fetchBalance(req.body.cpr, req.body.userID);
  //     const currencies = "";
  //     return SendResponse(res, 200, "Card Preferences", "");
  //   } catch (err) {
  //     return SendResponse(res, 500, "Card Preferences", "");
  //   }
  // },
  async fetchPreference(req, res) {
    let card = new cardEntity(req.body.cpr, req.body.userID);
    try {
      await card.fetchPreference();
      let data = card.data;
      return SendResponse(res, 200, "Card Preferences", data);
    } catch (e) {
      //return SendResponse(res, 500,"Internal Error",null)
      return SendResponse(res, 500, card.errorMessage, null);
    }
  },
  async setPreference(req, res) {
    console.log(req.body);
    let card = new cardEntity(req.body.entityId, req.body.userID);
    //  requestformat = {
    //            "ATM":"ALLOWED",
    //            "POS": "ALLOWED",
    //            "ECOM":"ALLOWED",
    //            "CONTACTLESS": "ALLOWED"
    //  }
    let cardStatus = req.body.cardStatus;

    try {
      //  console.log("here is card");
      // Promise.all([
      //   card.setPreference("ATM", cardStatus.ATM),
      //   card.setPreference("POS", cardStatus.POS),
      //   card.setPreference("ECOM", cardStatus.ECOM),
      //   card.setPreference("CONTACTLESS", cardStatus.CONTACTLESS),
      // ])
      //   .then(async () => {
      //     try{
      //         await card.fetchPreference();
      //         let data = card.data;
      //         return SendResponse(res, 200, "Preferences updated", data);
      //     }
      //     catch(err){
      //       console.log('heref')
      //       console.log(err)
      //       return SendResponse(res, 500, "Error while updating preferences");
      //     }

      //   })
      //   .catch((err) => {
      //     console.log('here')
      //     console.log(err)
      //     return SendResponse(res, 500, "Error while updating preferences");
      //   });
      // await  Promise.all([
      //     card.setPreference("ATM", cardStatus.ATM),
      //     card.setPreference("POS", cardStatus.POS),
      //     card.setPreference("ECOM", cardStatus.ECOM),
      //     card.setPreference("CONTACTLESS", cardStatus.CONTACTLESS),
      //   ])
      await card.setPreference("ATM", cardStatus.ATM);
      await card.setPreference("POS", cardStatus.POS);
      await card.setPreference("ECOM", cardStatus.ECOM);
      await card.setPreference("CONTACTLESS", cardStatus.CONTACTLESS);
      await card.fetchPreference();
      let data = card.data;
      return SendResponse(res, 200, "Preferences updated", data);
    } catch (e) {
      console.log(e);
      return SendResponse(res, 500, card.errorMessage, null);
    }
  },
  async cardDashboard(req, res) {
    try {
      let CPR = req.body.cpr,
        userID = req.body.userID;
      // const data = await m2pService.fetchBalance(req.body.cpr, req.body.userID);
      // let currencies = ["USD", "EUR", "AED", "GBP", "SAR", "BHD"];
      // let walletBalance = [];
      // for (let cur of currencies) {
      //   let curObj = {
      //     currency: cur,
      //     balance: data.result[cur],
      //     flag: await fetchBase64Image(`${__dirname}/../assets/flags/${cur.toLowerCase()}.png`),
      //   };
      //   walletBalance.push(curObj);
      // }
      let cardObject = new cardEntity(CPR, userID);
      let walletBalance = await cardObject.fetchBalance();
      let urls = await cardObject.fetchURLs();
      let userDocument = await User.findOne({ CPR });
      let cardDocument = await Card.findOne({ CPR });
      await cardObject.fetchAndUpdateCardStatus(userDocument, cardDocument);
      // let cardReplacementFees = {
      //   replacementFee: "10.000",
      //   replacementVatPercentage: "5",
      //   vatAmount: "0.3",
      //   replacementTotalFee: "20.000",
      // };
      let cardReplacementFees = {
        replacementFee: "0.000",
        replacementVatPercentage: "0",
        vatAmount: "0.0",
        replacementTotalFee: "0.000",
      };
      let response = {
        walletBalance,
        cardReplacementFees,
        urls,
        cardStatus: userDocument.status,
      };
      return SendResponse(res, 200, "", response);
    } catch (err) {
      console.log(`ERRNAME:${err.name} ERRMSG:${err.message}`);
      return SendResponse(res, 500, cardObject.errorMessage || "Internal Server Error");
    }
  },

  async changeCardStatus(req, res) {
    let card;

    try {
      let CPR = req.body.cpr,
        flag = req.body.flag,
        userID = req.body.userID;
      card = new cardEntity(CPR, userID);
      let cardData = await Card.findOne({ CPR });
      let userData = await User.findOne({ CPR });

      await card.changeCardStatus(flag, cardData.cards[cardData.cards.length - 1].kitNo);
      let data = card.data;
      if (data === "success") {
        // const cardList = await m2pService.getCardList(CPR, userID);
        // const cardsArray = convertCardDetailsFormat(cardList.result);
        // userData.status = cardsArray[0].cardStatus;
        // await userData.save();
        // cardData.cards = cardsArray;
        // await cardData.save();
        await card.fetchAndUpdateCardStatus(userData, cardData);
        data = { result: data, status: userData.status };
        return SendResponse(res, 200, "Card Status", data);
      }
    } catch (err) {
      console.log(`ERRNAME:${err.name} ERRMSG:${err.message}`);
      return SendResponse(res, 500, card.errorMessage || "Internal Server Error");
    }
  },
  async getPCIWidgetUrl(req, res) {
    let card;
    try {
      let { cpr: CPR, userID, type } = req.body;
      let cardDoc = await Card.findOne({ CPR }).lean();
      let userDoc = await User.findOne({ CPR }).lean();
      card = new cardEntity(CPR, userID);
      await card.getPCIWidgetUrl(type, userDoc, cardDoc);
      let data = card.data;
      return SendResponse(res, 200, `${type} URL`, data);
    } catch (err) {
      console.log(`ERRNAME:${err.name} ERRMSG:${err.message}`);
      return SendResponse(res, 500, card.errorMessage || "Internal Server Error");
    }
  },
  async replaceCard(req, res) {
    let card;
    try {
      let cpr = req.body.cpr;
      let userID = req.body.userID;
      let address = req.body.address;
      card = new cardEntity(cpr, userID);
      await card.replaceCard(address);
      return SendResponse(res, 200, "Replace Status", card.data);
    } catch (err) {
      //console.log(`ERRNAME:${err.name} ERRMSG:${err.message}`);
      return SendResponse(res, 500, card.errorMessage || "Internal Server Error");
    }
  },
  async addKitNo(req, res) {
    try {
      let no = 6900000000;
      let arr = [];
      for (let i = 0; i < 100; i++) {
        arr.push({ kitNo: no++ });
      }
      await kitNumbers.insertMany(arr);
      console.log("success");
      res.status(200).json({ message: "success" });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: "failed" });
    }
  },
};

module.exports = cardController;
