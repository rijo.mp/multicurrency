const smsOtpSchema = require('../models/smsOtpSchema')
const Sms = require('../classes/sms.class')

class SmsOtp extends Sms{
    constructor(userID,cpr,to,type){
        super(to,type)
        this.userID = userID;
        this.cpr = cpr;
    
    }
    generateOtp(){
        return Math.floor(100000 + Math.random() * 899999);
    }
    sendOtp(server){
        return new Promise((resolve, reject) => {
            try {
                smsOtpSchema.findOne({ userID: this.userID }).then(async(doc) => {
                    let currentTime = new Date(Date.now())
                    if (doc) {
                        console.log(doc.time < currentTime.setTime(currentTime.getTime() - 30 * 60 * 1000))
                        console.log(doc.retryCount >= 3);
                        doc.retryCount += 1;
                        if(doc.retryCount > 3){
                            if((doc.time > currentTime.setTime(currentTime.getTime() - 1 * 60 * 1000))){
                                return resolve({retryLimitReached:true})
                                    
                            }
                            else{
                                doc.retryCount = 0;
                            }
                               
                           
                        }
                        this.otp = this.generateOtp()
                        doc.otp = this.otp
                        doc.mobile = this.number;
                        doc.time = new Date(Date.now());
                        doc.type = this.type
                        doc.status = false;
                        doc.userID = this.userID;
                        doc.cpr = this.cpr
                        doc.verifyCount = 0;
                        this.data = this.setSms()
                        if(server == 'server1'){
                            await this.sendServer1()
                            
                        }
                        else{
                            await this.sendServer2() 
                           
                        }
                        
                        await doc.save();
                        resolve({retryLimitReached:false})
                    }
                    else {

                        let doc = new smsOtpSchema();
                        this.otp = this.generateOtp()
                        doc.otp = this.otp
                        doc.mobile = this.number;
                        doc.time = new Date(Date.now());
                        doc.type = this.type
                        doc.status = false;
                        doc.userID = this.userID;
                        doc.cpr = this.cpr
                        doc.verifyCount = 0;
                        this.data = this.setSms()
                        if(server == 'server1'){
                            await this.sendServer1()
                            doc.retryCount = 0;
                        }
                        else{
                            await this.sendServer2()
                            doc.retryCount = 0; 
                        }
                        
                        await doc.save();
                        resolve({retryLimitReached:false})
                       
                    }
                })
            }
            catch (ex) {
                reject(ex)
            }
        })
    }
}

module.exports = SmsOtp;