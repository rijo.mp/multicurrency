const bfcAxios = require("../interceptors/interceptor.bfc");

let serviceBfc = {
  getExchangeRateByRSCODE(userID) {
    return new Promise(async (resolve, reject) => {
      try {
        let API = "GetRatesByRSCODE",
          RSCode = "IFT";
        let xmlObject = `<?xml version="1.0" encoding="utf-8"?>
              <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                <soap:Body>
                  <GetRatesByRSCODE xmlns="http://fossilapi.com/">
                    <RSCODE>${RSCode}</RSCODE>
                  </GetRatesByRSCODE>
                </soap:Body>
              </soap:Envelope>`;
        let data = await bfcAxios.post("/esb/BH/11/RatesApiFossil", xmlObject, { params: { API, userID } });
        resolve(data);
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  },
};

module.exports = serviceBfc;

// async function test() {
//   try {
//     let data = await serviceBfc.getExchangeRateByRSCODE();
//     console.log(data);
//   } catch (err) {
//     console.log(err);
//   }
// }
// test();
