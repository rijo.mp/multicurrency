const Enum = require("enum");

const cardEnum = new Enum({
  cardList: "cardNumber",
  kitList: "kitNo",
  expiryDateList: "expiryDate",
  cardStatusList: "cardStatus",
  cardTypeList: "cardType",
  networkTypeList: "networkType",
});

module.exports = cardEnum;
