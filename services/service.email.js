const dotenv = require("dotenv");
dotenv.config();

const NM = require('../config/emailConfig');

const Notify = {
  sendEmail: async (obj) => {
    console.log("here...");
    return new Promise((resolve, reject) => {
      NM.sendMail({
        from: process.env.mailFrom,
        to: obj.email == undefined ? obj["to"] : obj["email"],
        subject: obj.subject,
        html: obj.html,
      }).then((ack) => {
        if (ack["accepted"].length > 0) resolve("Mail Sent");
        else reject("Failed");
      });
    });
  },
  sendEmailBCC: async (obj) => {
    return new Promise((resolve, reject) => {
      NM.sendMail({
        from: process.env.mailFrom,
        to: obj.email == undefined ? obj["to"] : obj["email"],
        bcc: obj.bcc,
        subject: obj.subject,
        html: obj.html,
      }).then((ack) => {
        if (ack["accepted"].length > 0) resolve("Mail Sent");
        else reject("Failed");
      });
    });
  },
  sendEmailWithAtachment: async (obj) => {
    return new Promise((resolve, reject) => {
      NM.sendMail({
        from: process.env.mailFrom,
        to: obj.email == undefined ? obj["to"] : obj["email"],
        subject: obj.subject,
        html: obj.html,
        attachments: [
          {
            filename: obj.filename,
            path: obj.filePath,
          },
        ],
      }).then((ack) => {
        if (ack["accepted"].length > 0) resolve("Mail Sent");
        else reject("Failed");
      });
    });
  },
};

module.exports = Notify;
