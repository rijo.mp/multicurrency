const mongoose = require("mongoose");

let m2pRequestResponseSchema = new mongoose.Schema(
  {
    userID: {
      type: String,
      required: true,
    },

    name: String,
    url: String,
    timeRequired: String,
    timeStamp: Date,
    request: String,
    response: String,
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("m2pRequestResponse", m2pRequestResponseSchema);
