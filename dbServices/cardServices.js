const m2pService = require("../services/services.m2p");
const { convertCardDetailsFormat } = require("../utils/cardUtils");
// const User = require("../models/userSchema");
// const Card = require("../models/cardSchema");

let cardServices = {
  updateCarddataInDB(CPR, userID, userModel, cardModel) {
    return new Promise(async (resolve, reject) => {
      try {
        // console.log(cardsArray, cardModel.cards);
        const data = await m2pService.getCardList(CPR, userID);
        const cardsArray = convertCardDetailsFormat(data.result);
        // let userModel = await User.findOne({ CPR });
        // let cardModel = await Card.findOne({ CPR });
        for (let card of cardsArray) {
          let flag = true;
          for (let cardDB of cardModel.cards) {
            if (card.kitNo === cardDB.kitNo) {
              cardDB.cardStatus = card.cardStatus;
              flag = false;
              break;
            }
          }
          if (flag) {
            cardModel.cards.push(card);
          }
          if (card.cardStatus != "REPLACED") {
            userModel.status = card.cardStatus;
          }
        }
        // console.log(cardModel.cards, userModel.status);
        await userModel.save();
        await cardModel.save();
        resolve();
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  },
};

module.exports = cardServices;
