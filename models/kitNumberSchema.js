const mongoose = require("mongoose");

let kitNumberSchema = new mongoose.Schema(
  {
    kitNo: {
      type: Number,
      required: true,
    },
    status: {
      type: Boolean,
      default: false,
    },
    // time    :   Date,
    userID: String,
    entityId: String,
    isInvalid: {
      type: Boolean,
      default: false,
    },
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("kitNumbers", kitNumberSchema);
