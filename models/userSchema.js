const mongoose = require("mongoose");

const emailSchema = new mongoose.Schema(
  {
    ID: {
      type: String,
    },
    verified: {
      type: Boolean,
      default: false,
    },
    retryCount: {
      type: Number,
      default: 0,
    },
  },
  { _id: false }
);

const addressSchema = new mongoose.Schema(
  {
    addressCategory: {
      type: String,
      default: "DELIVERY",
    },
    AHOname: {
      type: String,
      default: "",
    },
    AHOnumber: {
      type: String,
      default: "",
    },
    floorNo: {
      type: String,
      default: "",
    },
    buildingNo: {
      type: String,
      default: "",
    },
    roadNo: {
      type: String,
      default: "",
    },
    blockNo: {
      type: String,
      default: "",
    },
  },
  { _id: false }
);

const UserSchema = new mongoose.Schema(
  {
    entityId: {
      type: String,
      required: true,
    },
    CPR: {
      type: String,
      required: true,
    },
    userID: {
      type: String,
      required: true,
    },
    title: String,
    firstName: {
      type: String,
      // required : true,
    },
    lastName: {
      type: String,
      // required : true,
    },
    nameOnCard: String,
    gender: String,
    DOB: String,
    phoneNo: String,
    isFirstAddmoney: {
      type: Boolean,
      default: true,
    },
    annualSubRenewDate: {
      type: Date,
    },
    status: {
      //Card Status:Active/Blocked/Locked
      type: String,
      default: "NO CARD",
    },
    emailInfo: emailSchema,
    addressInfo: [addressSchema],
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("User", UserSchema);
