const axiosNetsys = require("../interceptors/interceptor.netsys");
const urls = require("../enums/netsysURLsEnum");

let netsysService = {
  getCustomer(cpr) {
    return new Promise(async (resolve, reject) => {
      try {
        let request = {
          Identification: {
            Type: "CPR",
            Value: cpr,
          },
        };
        let customer = await axiosNetsys.post(urls.getCustomer.value, request);
        // this.customer = data;
        resolve(customer);
      } catch (ex) {
        reject(ex);
      }
    });
  },
  getApplication(cpr) {
    return new Promise(async (resolve, reject) => {
      try {
        let request = {
          CPR: cpr,
        };
        let application = await axiosNetsys.post(urls.getApplication.value, request);
        // this.applicationID = data[0].ApplicationID;
        // this.application = data[0];
        resolve(application);
      } catch (ex) {
        reject(ex);
      }
    });
  },
};

module.exports = netsysService;
