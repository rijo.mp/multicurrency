let common = {
  SendResponse: function (res, code, message, data /*logID*/) {
    // if (logID) {
    //     LogDatabaseSchema.findOne({ logID: logID }, function (err, doc) {
    //         if (err)
    //             console.log(err)
    //         if (doc) {
    //             doc.response = (data ? JSON.stringify(data) : "")
    //             //doc.userID = doc.userID ? doc.userID : (req.user!= undefined ? req.user.userID : '')
    //             doc.status = code;
    //             doc.save()
    //         }
    //     })
    // }

    return res.status(200).json({
      status: {
        code: code,
        message: message,
      },
      data: data ? data : null,
    });
  },
};

module.exports = common;
