const express = require("express");
const router = express.Router();
const cardController = require("../controllers/cardController");
const transactionController = require("../controllers/transactionsController");

router.post("/getCardDetails", cardController.getMulticurrencyCardDetails);
router.post("/applyCard", cardController.applyCard);
router.post("/addKitNo", cardController.addKitNo);
router.post("/cardDashboard", cardController.cardDashboard);
// router.post("/getDashboardData", cardController.getDashboardData);
router.post("/fetchPreference", cardController.fetchPreference);
router.post("/setPreference", cardController.setPreference);
router.post("/changeCardStatus", cardController.changeCardStatus);
router.post("/getWebViews", cardController.setPreference);
router.post("/getPCIWidgetUrl", cardController.getPCIWidgetUrl);
router.post("/replaceCard", cardController.replaceCard);
router.post("/fetchAmountSuggestions", transactionController.fetchAmountSuggestions);
router.post("/fetchTotalAmount", transactionController.fetchTotalAmount);
router.post("/createTransaction", transactionController.createTransaction);
router.post("/updateTransaction", transactionController.updateTransaction);
// router.post("/getPaymentMethods", transactionController.getPaymentMethods);

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

module.exports = router;
