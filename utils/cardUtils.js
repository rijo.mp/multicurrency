const cardEnum = require("../enums/cardEnum");
const addressEnum = require("../enums/addressEnum");
const imageToBase64 = require("image-to-base64");
const crypto = require("crypto-js");

module.exports.convertCardDetailsFormat = function (cardData) {
  const entries = Object.entries(cardData);
  let cardsArray = [];
  for (let i in entries[0][1]) {
    let temp = {};
    for (let j of entries) {
      temp[cardEnum[j[0]].value] = j[1][i];
    }
    cardsArray.push(temp);
  }
  return cardsArray;
};

module.exports.convertAddressFormat = function (addressData) {
  let address = {};
  for (let i of Object.entries(addressData)) {
    if (addressEnum[i[0]]) {
      address[addressEnum[i[0]].value] = i[1];
      if (i[0] === "AddressType" && i[1] === "Home") address[addressEnum[i[0]].value] = "House";
    }
  }
  return address;
};

module.exports.fetchBase64Image = function (path) {
  return new Promise(async (resolve, reject) => {
    try {
      const base64Image = await imageToBase64(path);
      resolve(base64Image);
    } catch (err) {
      console.log(err.message);
      reject(err);
    }
  });
};

module.exports.generateToken = function (...arr) {
  return new Promise(async (resolve, reject) => {
    try {
      let token = await crypto.AES.encrypt(JSON.stringify(arr), "BFC_Multicurrency").toString();
      resolve(token);
    } catch (err) {
      console.log(err);
      reject(err);
    }
  });
};

// (async () => {
//   let data = await module.exports.generateToken(1, 2, 3);
//   console.log(data);
// })();

// module.exports.updateCarddataInDB = function (CPR, userID) {
//   return new Promise(async (resolve, reject) => {
//     try {
//       // console.log(cardsArray, cardModel.cards);
//       const data = await m2pService.getCardList(CPR, userID);
//       const cardsArray = convertCardDetailsFormat(data.result);
//       let userModel = await User.findOne({ CPR });
//       let cardModel = await Card.findOne({ CPR });
//       for (let card of cardsArray) {
//         let flag = true;
//         for (let cardDB of cardModel.cards) {
//           if (card.kitNo === cardDB.kitNo) {
//             cardDB.cardStatus = card.cardStatus;
//             flag = false;
//             break;
//           }
//         }
//         if (flag) {
//           cardModel.cards.push(card);
//         }
//         if (card.cardStatus != "REPLACED") {
//           userModel.status = card.cardStatus;
//         }
//       }
//       console.log(cardModel.cards, userModel.status);
//       await userModel.save();
//       await cardModel.save();
//       resolve();
//     } catch (err) {
//       console.log(err);
//       reject(err);
//     }
//   });
// };
